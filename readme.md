# TJS-Helloworld

Small project to start exploring with typescript-json-schema

## Install

Tested on Ubuntu 20.04 LTS

Clone this repository to your pc

Run `npm i` to install

Then `npm run dev` and it should compile the code and generate JSON Schema object in dist/json-schema directory.
