import fs from 'fs';
import path from "path";
import * as tjs from "typescript-json-schema";

const settings: tjs.PartialArgs = {
    required: true
};
const compilerOptions: tjs.CompilerOptions = {
    strictNullChecks: true
};

const schemaWriter = (filename: string, ifnames: string[]) => {
    const basePath = ["src","interfaces"];
    const program = tjs.getProgramFromFiles(
        [path.resolve(...basePath,filename)],
        compilerOptions
    );
    for (const ifname of ifnames) {
        const schema = tjs.generateSchema(
            program, 
            ifname,
            settings
        );
        const JSONSchema = JSON.stringify(schema, null, 4);
        const savepath = path.join(
            __dirname,'json-schemas',
            (filename.split('.')[0]+'.json')
        );
        fs.writeFileSync(
            savepath,
            JSONSchema,
            { encoding: 'utf-8' }
        );
    }
}
export default schemaWriter;