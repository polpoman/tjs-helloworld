
import { resolve } from "path";
import * as tjs from "typescript-json-schema";
import schemaWriter from "./schema-writer";

const settings: tjs.PartialArgs = {
    required: true
};

const compilerOptions: tjs.CompilerOptions = {
    strictNullChecks: true
};

const basePath = "./srce";

const program = tjs.getProgramFromFiles(
    [resolve("src","interfaces","square.ts")],
    compilerOptions,
    basePath
)

const schema = tjs.generateSchema(program, "Square", settings);

const main = () => {
    console.log('Program started');
    schemaWriter('square.ts', ['Square']);
    console.log('Program ended');
}

export default main();