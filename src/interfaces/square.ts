export interface Square {
    width: number;
    height: number;
}